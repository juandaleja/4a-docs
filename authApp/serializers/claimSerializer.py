from authApp.models.claim import Claim
from rest_framework import serializers

class ClaimSerializer(serializers.ModelSerializer):
    class Meta:
        model = Claim
        fields = ['content', 'issue', 'user', 'date']
    
    def create(self, validated_data):
        claimInstance = Claim.objects.create(**validated_data)
        return claimInstance