from django.conf import settings
from rest_framework import generics, status
from rest_framework.response import Response
from authApp.models.claim import Claim
from authApp.serializers.claimSerializer import ClaimSerializer

class ClaimDetailView(generics.RetrieveAPIView):
    queryset = Claim.objects.all()
    serializer_class = Claim

    def get(self, request, *args, **kwargs):
        claimvar = Claim.objects.all()
        claimserializer = ClaimSerializer(claimvar, many=True)
        
        return Response(claimserializer.data)