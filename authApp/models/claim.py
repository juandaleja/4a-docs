from django.db import models
from .user import User
import json


class ClaimManager():
    def create_claim(self, user_id, date, content, issue, id):
        if not user_id:
            raise ValueError('you need user_id')
        claim = self.model(id=id)
        claim.set_date(date)
        claim.set_content(content)
        claim.set_issue(issue)
        claim.set_user_id(user_id)
        claim.save(using=self._db)
        return claim
class Claim(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='claim', on_delete=models.CASCADE)
    date = models.DateField()
    content = models.CharField(max_length=500)
    issue = models.CharField(max_length=100)

    def save(self, **kwargs):
        super().save(**kwargs)
        
    objects = ClaimManager()
    USERNAME_FIELD = 'id'